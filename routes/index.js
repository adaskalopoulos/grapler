var path = require('path');
var fs = require('fs');
var express = require('express');
var router = express.Router();
var codes = require(path.resolve('.', 'codes.js'));
var exec = require('child_process').exec;
var bunyan = require('bunyan');
var log = bunyan.createLogger({
    name: "grapler"
});
var rimrafPromise = require('rimraf-promise');

function create() {
    return new Promise(function(resolve, reject) {
        exec("cordova create /cordova/hello com.example.hello HelloWorld", function(error, stdout, stderr) {
            log.info(stdout);
            if (error) {
                log.error(error.message);
                return reject(error);
            } else {
                return resolve();
            }
        });
    });
}

function addPlatform() {
    return new Promise(function(resolve, reject) {
        process.chdir('/cordova/hello');
        exec("cordova platform add android", function(error, stdout, stderr) {
            log.info(stdout);
            if (error) {
                log.error(error.message);
                return reject(error);
            } else {
                return resolve();
            }
        });
    });
}

function build() {
    return new Promise(function(resolve, reject) {
        process.chdir('/cordova/hello/platforms/android');
        exec("cordova build android", function(error, stdout, stderr) {
            log.info(stdout);
            if (error) {
                log.error(error.message);
                return reject(error);
            } else {
                return resolve();
            }
        });
    });
}

router.get('/', function(req, res, next) {
    res.render('index');
});

router.post('/build', function(req, res, next) {
    log.info('Great success');
    res.json(200, {
        code: codes.OK.code,
        message: codes.OK.message
    });
    //rimrafPromise('/cordova/hello').then(function() {
    //    return create().then(function() {
    //        return addPlatform();
    //    }).then(function() {
    //        return build();
    //    }).done(function() {
    //        res.send('apPrize API is running');
    //    }, function(err) {
    //        if (err && err.message) {
    //            if (err.message.indexOf("Command failed: Path already exists and is not empty") > 0) {
    //                res.json(500, {
    //                    code: codes.EXISTING_PATH.code,
    //                    message: codes.EXISTING_PATH.message
    //                });
    //            }
    //        } else {
    //            res.json(500, {
    //                code: codes.INTERNAL_SERVER_ERROR.code,
    //                message: codes.INTERNAL_SERVER_ERROR.message
    //            });
    //        }
    //    })
    //});
});

module.exports = router;