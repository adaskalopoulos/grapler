function define(name, value, msg) {
    Object.defineProperty(exports, name, {
        value: {
            code: value,
            message: msg
        }
    });
}

define('INTERNAL_SERVER_ERROR', 1, "Internal server error");
define('EXISTING_PATH', 2, "Path already exists and is not empty");
define('OK', 3, "Great success");

